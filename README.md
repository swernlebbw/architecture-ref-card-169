# Card Game

Architecture Ref. Card 169

Sergio Wernle

Mein Repository wurde von folgendem Link geforkt: gitlab.com/bbwin/architecture-ref-card-169
Dockerfile, welches einen ausführbaren Docker-Container generiert.

Dockerfile für Java11:
```
FROM maven:3-openjdk-11-slim as builder
COPY src /src
COPY pom.xml /
RUN mvn -f pom.xml clean package
FROM adoptopenjdk/openjdk11:alpine-jre
COPY --from=builder /target/*.jar app.jar
ENTRYPOINT ["java","-jar","/app.jar"]
```
Das Repository habe ich auf AWS ECR erstellt unter dem Namen "bbw".

Gitlab-ci.yml:

```
`image: docker:23.0.4

variables:
  DOCKER_HOST: tcp://docker:2375
  DOCKER_TLS_CERTDIR: ""

services:
  - docker:23.0.4-dind

package:
  stage: build
  before_script:
    - apk add --no-cache py3-pip
    - pip install awscli
    - aws --version

    - aws ecr get-login-password | docker login --username AWS --password-stdin $CI_AWS_ECR_REGISTRY

  script:
    - docker build --cache-from $CI_AWS_ECR_REGISTRY/$CI_AWS_ECR_REPOSITORY_NAME:latest -t $CI_AWS_ECR_REGISTRY/$CI_AWS_ECR_REPOSITORY_NAME:latest .
    - docker push $CI_AWS_ECR_REGISTRY/$CI_AWS_ECR_REPOSITORY_NAME:latest
```

`

Danach sollte der Runner gestartet werden, jedoch funktioniert dieser bei mir nicht.
Folgender Fehler kam bei mir:
```
User validation required
To use free units of compute on shared runners, you'll need to validate your account with a credit card. If you prefer not to provide one, you can run pipelines by bringing your own runners and disabling shared runners for your project. This is required to discourage and reduce abuse on GitLab infrastructure. GitLab will not charge your card, it will only be used for validation.
```
Danach habe ich einen Cluster mit dem Namen "bbw" erstellt.

Die Task Definition kann man ebenfalls in diesem Menü erstellen.
Für Task-Rolle und Task-Ausführungsrolle sollten wir "LabRole" auswählen.

Beim erstellen des Services gab es folgender Fehler:
```
Beim Bereitstellen von bbwsergio ist ein Fehler aufgetreten
Resource handler returned message: "Error occurred during operation 'ECS Deployment Circuit Breaker was triggered'." (RequestToken: da39be77-a046-b125-4db4-883598f94f08, HandlerErrorCode: GeneralServiceException)
```

Leider ist vieles nicht wie gewollt verlaufen.
